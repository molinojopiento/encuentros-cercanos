require 'test_helper'

class ArchivosControllerTest < ActionController::TestCase
  test "should get subir" do
    get :subir
    assert_response :success
  end

  test "should get listar" do
    get :listar
    assert_response :success
  end

  test "should get borrar" do
    get :borrar
    assert_response :success
  end

  test "should get guardar" do
    get :guardar
    assert_response :success
  end

end
